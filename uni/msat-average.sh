#!/bin/bash
#
# *** msat-average.sh ***
#
# Dieses Skript filtert ASCII-Daten des Metis MSAT so, dass
# der Dateikopf und jede Zeile, welche Mittelwerte enthält, in eine Datei
# gespeichert werden.
# Die Daten werden in der Datei 'data.txt' erwartet, die Ausgabe erfolgt in
# die Datei 'output.txt'.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de, 14.04.2015
#
# Schreibe die erste Zeile der Datei data.txt in output.txt.
#
head -n 1 data.txt > output.txt
#
# Extrahiere alle Zeilen mit dem Stichwort 'average' und hänge sie
# an output.txt an.
#
cat data.txt | grep average >> output.txt
