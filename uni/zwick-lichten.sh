#!/bin/bash
#
# *** zwick-lichten.sh ***
#
# Dieses Skript filtert ASCII-Daten der Zugprüfmaschine ZWICK 1476 so, dass
# der Dateikopf und jeder 100. Wert in eine Datei gespeichert werden.
# Die Daten werden in der Datei 'data.txt' erwartet, die Ausgabe erfolgt in
# die Datei 'output.txt'.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de, 09.12.2014
#
# Schreibe die ersten 6 Zeilen und ab der 7. jede 100. Zeile der Datei
# data.txt in output.txt.
#
sed -n '1,6p; 7~100p' data.txt > output.txt
