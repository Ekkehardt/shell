#! /bin/bash
# *** audioconvert.sh ***
# Description: "Convert many audio files from different formats to mp3. Then all
#       mp3 are recodes to standard settings"
# Version: 0.5.0
################################################################################
# This script reads audio files from the current directory and its subfolders
#       and converts them to mp3, regardless of source format, using lame.
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2024-01-29
################################################################################
# *ONGOING* ->  Preparation for 1.0.0
#   Commit: "audioconvert: <TEXT>"
#     TODO: make it more flexible!
################################################################################
#
# TODO: Folder, where audio files are searched for
#
find ./ -iname '*.wav' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.wav' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.ogg' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.ogg' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.flac' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.flac' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.fla' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.fla' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.m4a' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.m4a' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.wma' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.wma' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.opus' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.opus' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.mp4' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.mp4' -print0 | xargs -0 -n1 -P4 rm
#
find ./ -iname '*.aac' -print0 | xargs -0 -n1 -P4 lame --preset standard
find ./ -iname '*.aac' -print0 | xargs -0 -n1 -P4 rm
#
for i in *.mp3; do lame --preset standard "$i" -o "`basename "$i" .mp3`_medium.mp3"; done;
#
echo "All conversions done."
