#! /bin/bash
# *** photowizard.sh ***
# Description: "Automate ImageMagick for lots of photos."
# Version: 0.5.0
################################################################################
# This script reads images from an 'input' folder and its subfolders, applies
#       some tweaks regarding white balance, despeckle and sharpen filters, and
#       saves the altered images to an 'output' folder with the same subfolders
#       as in the input tree.
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-03-09
################################################################################
# *ONGOING* ->  Preparation for 1.0.0
#   Commit: "photowizard: <TEXT>"
#     TODO: make it more flexible!
################################################################################
#

# Folders
input_dir="input"
output_dir="output"

# Create output folder and subfolders, if not present
find "$input_dir" -type d -exec sh -c 'mkdir -p "$0/${1#*/}"' "$output_dir" '{}' \;

# white balance, noise reduction, sharpen
find "$input_dir" -type f -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" -o -iname "*.gif" | while read -r file
do
    input_path="$file"
    output_path="${file/$input_dir/$output_dir}"  # Swap input folder with output folder

    convert "$input_path" -auto-gamma -auto-level -normalize -despeckle -sharpen 2x1.5 "$output_path"
done
