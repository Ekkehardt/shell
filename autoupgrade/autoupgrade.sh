#! /bin/bash
# *** autoupgrade.sh ***
# Description: "Complete upgrade of Ubuntu machines."
# Version: 0.7.5
################################################################################
# This script fetches and installs all available upgrades through apt, the
#       Tex Live manager, flatpak and the snap system. After application it
#       carries out a clean-up.
#
# Should reside in ~/scripts. Will create a logfile in ~/scripts/mylogs.
#
# May be enabled with 'sudo crontab -e';
#       '@reboot /root/scripts/autoupgrade.sh'
#       '37 1 * * * /root/scripts/autoupgrade.sh'
#       Or run via alias: alias full-upgrade='$HOME/scripts/autoupgrade.sh'
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-03-08
################################################################################
# *ONGOING* ->  Preparation for 1.0.0
#   Commit: "autoupgrade: use only necessary packet managers, code optimized, ..."
#     TODO: Tidy up log output!
#     TODO: Cleanup for flat?
################################################################################
#
## Variables init
VERSION="0.7.5"
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
LOGFILE="/home/user/scripts/mylogs/autoupgrade.log"
N=0
#
## Functions
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update Variables
timestamp
MACHINE=$(hostname)
USER=$(whoami)
LOGFILE=$HOME/scripts/mylogs/autoupgrade.log
#
## Make Logdir, if not already done # TODO: rewrite!!!
DIR="$HOME/scripts/mylogs"
if [ -d "$DIR" ]; then
  echo "Logdir already present." &> /dev/null
  ### Nothing to do ###
  else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p "$HOME/scripts/mylogs/"
  echo "Logdir created." &> /dev/null
fi
#
## Check if repos are online
while true; do ping -c1 www.ubuntu.com &> /dev/null && break; done
#
{ ## Logfile init # TODO: sudo?!
  echo " "
  echo "Script autoupgrade V. $VERSION started by $USER@$MACHINE on $TIMESTAMP."
  echo "================================================================================"
  echo " "
  } > "$LOGFILE"
sudo touch "$LOGFILE"
#
## We assume apt is always necessary on a deb based machine
(( N++ ))
{ ## Apt stuff # TODO: sudo?!
  echo "$N.: Upgrade packages installed through apt/dpkg"
  echo "-----------------------------------------------"
  # Update package list
  sudo apt-get update
  echo
  # Upgrade all debs
  sudo apt-get dist-upgrade -y
  echo
  # Remove debs no longer in repos
  sudo apt-get autoremove -y
  echo
  # Clean local package cache
  sudo apt-get autoclean -y
  timestamp
  echo "$TIMESTAMP"
  echo
  # Clean old configs
  sudo dpkg --purge "$(dpkg -l | grep ^rc | awk '{print $2}')" &> /dev/null
} | tee -a "$LOGFILE"
#
sleep 5 # Wait for logfile operation to finish
## Only if snap is present and snaps are installed, refresh them
if command -v snap &> /dev/null; then
  if [[ -n "$(sudo snap list)" ]]; then
  (( N++ ))
  { ## Upgrade snaps # TODO: sudo?!
    echo "$N.: Upgrade snaps"
    echo "-----------------------------------------------"
    sudo snap refresh &>> /dev/null         ### logs blurb! why?
    # sudo snap refresh --list
    snap changes | tail
    # sudo rm /var/lib/snapd/cache/*        ### Does not exist. Where else cache?
    # sudo sh -c 'rm -rf /var/lib/snapd/cache/*'
    # sudo find /var/lib/snapd/cache/ -exec rm -v {} \; ### TODO: Other cleanup?
    ### Cache in userhome?
    echo
    timestamp
    echo "$TIMESTAMP"
    echo
    ## Removes old revisions of snaps
    ## TODO: CLOSE ALL SNAPS BEFORE RUNNING THIS
    #set -eu
    #
    # LANG=C snap list --all | awk '/disabled/{print $1, $3}' |#
      #while read snapname revision; do
          #  snap remove "$snapname"  --revision="$revision"
        #done
  } | tee -a "$LOGFILE"
  else echo "No snaps installed." | tee -a "$LOGFILE"
  fi
else
  echo "Snap is not installed." | tee -a "$LOGFILE"
fi
#
sleep 5 # Wait for logfile operation to finish
#
## Only if flatpak is present and flatpaks are installed, refresh them
if command -v flatpak &> /dev/null; then
  if [[ -n "$(flatpak list)" ]]; then
  (( N++ ))
  { ## Upgrade flatpaks # TODO: sudo?!
    echo "$N.: Upgrade flatpaks"
    echo "--------------------" # TODO limit output to recent (head?)!
    flatpak remote-ls --updates
    sudo flatpak update -y &> /dev/null    ### logs blurb! 2part! # TODO: Test again!
    echo
    timestamp
    echo "$TIMESTAMP"
    echo
    # Flatpak Cleanup
    flatpak uninstall --unused -y &> /dev/null
    rm -rv /var/tmp/flatpak-cache-* &> /dev/null # TODO short log?
    rm -rv "$USER"/home/.cache/flatpak &> /dev/null
    echo "Flatpak cache cleaned."
    echo
    # flatpak repair ?
    # update_flatpak_cli.py     ### Cite Source/©! Startable in bash!!
  } | tee -a "$LOGFILE"
  else
    echo "No flatpaks installed." | tee -a "$LOGFILE"
  fi
else
  echo "Flatpak is not installed." | tee -a "$LOGFILE"
fi
#
sleep 5 # Wait for logfile operation to finish
#
## Only if texlive is present, refresh the tlmgr and tex packages
#
if command -v tlmgr &> /dev/null; then
  (( N++ ))
  { ## Upgrade TeX Live # TODO: sudo?!
  echo "$N.: Upgrade TeX Live"
  echo "-----------------------------------------------"
  sudo tlmgr update --self --all
} | tee -a "$LOGFILE"
else
  echo "Texlive is not installed."
fi
sleep 5 # Wait for logfile operation to finish
## Log footer
{ ## Finish note and time
  echo " "
  timestamp
  echo "$TIMESTAMP"
  echo " "
  # TODO: Cleanup?
  #
  echo "Software upgrade finished."
  echo " "
}| tee -a "$LOGFILE"
#
sleep 5 # Wait for logfile operation to finish
#
{ ## Check if reboot is necessary # TODO: sudo?!
  if [ -f /run/reboot-required ]; then
        echo "*** REBOOT REQUIRED ***"
    else
          echo "No reboot required."
  fi
} | tee -a "$LOGFILE"
#
