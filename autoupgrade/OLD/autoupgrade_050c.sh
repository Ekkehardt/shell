#! /bin/bash
# *** autoupgrade.sh ***
# Description: "Complete upgrade of Ubuntu machines."
# Version: 0.5.0c
################################################################################
# This script fetches and installs all available upgrades through apt, the
#       Tex Live manager, flatpak and the snap system. After application it
#       carries out a clean-up.
#
# Should reside in ~/scripts. Will create a logfile in ~/scripts/mylogs.
#
# May be enabled with 'sudo crontab -e';
#       '@reboot /root/scripts/autoupgrade.sh'
#       '37 1 * * * /root/scripts/autoupgrade.sh'
#       Or run via alias: alias full-upgrade='$HOME/scripts/autoupgrade.sh'
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-02-07
################################################################################
# TODO: Relative Paths?
# TODO: Cleanup for flat?
# TODO: Root-Check w/warning and break bc whoami?
# TODO: Check and only do what's neccessary?
################################################################################
#
## Variables init
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
LOGFILE="/home/user/scripts/mylogs/autoupgrade.log"
N=0
#
## Functions
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update Variables
timestamp
MACHINE=$(hostname)
USER=$(whoami)
LOGFILE=$HOME/scripts/mylogs/autoupgrade.log
#
## Make Logdir, if not already done
DIR="$HOME/scripts/mylogs"
if [ -d "$DIR" ]; then
  echo "Logdir already present." >> /dev/null
  ### Nothing to do ###
  else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p "$HOME/scripts/mylogs/"
  echo "Logdir created." >> /dev/null
fi
#
## Check if repos are online
while true; do ping -c1 www.ubuntu.com &> /dev/null && break; done
#
## Logfile init
echo " "
echo "Last software upgrade started by $USER@$MACHINE $TIMESTAMP." > "$LOGFILE"
echo "================================================================================" >> "$LOGFILE"
echo " " >> "$LOGFILE"
#
## Apt stuff
(( N++ ))
echo "$N.: Upgrade packages installed through apt/dpkg" >> "$LOGFILE"
echo "-----------------------------------------------" >> "$LOGFILE"
# Update package list
sudo apt-get update >> "$LOGFILE" # FIXME tee!
echo " " >> "$LOGFILE"
# Upgrade all debs
sudo apt-get dist-upgrade -y >> "$LOGFILE" # FIXME tee!
echo " " >> "$LOGFILE"
# Remove debs no longer in repos
sudo apt-get autoremove -y >> "$LOGFILE" # FIXME tee!
echo " " >> "$LOGFILE"
# Clean local package cache
sudo apt-get autoclean -y >> "$LOGFILE" # FIXME tee!
timestamp
echo "$TIMESTAMP" >> "$LOGFILE"
echo " " >> "$LOGFILE"
# Clean old configs
# sudo dpkg --purge `dpkg -l | grep ^rc | awk '{print $2}'` ### LOG! # TODO: make it work!
#
## Upgrade snaps
(( N++ ))
echo "$N.: Upgrade snaps" >> "$LOGFILE"
echo "-----------------------------------------------" >> "$LOGFILE"
sudo snap refresh >> /dev/null         ### logs blurb! why? # FIXME: tee!
sudo snap refresh --list >> "$LOGFILE" # FIXME: tee!
snap changes >> "$LOGFILE"
# sudo rm /var/lib/snapd/cache/*        ### Does not exist. Where else cache?
# sudo sh -c 'rm -rf /var/lib/snapd/cache/*'
# sudo find /var/lib/snapd/cache/ -exec rm -v {} \;
                                        ### Other cleanup?
# echo "Output of 'snap refresh' could not be redirected to log file!" >> "$LOGFILE"
echo " " >> "$LOGFILE"
timestamp
echo "$TIMESTAMP" >> "$LOGFILE"
echo " " >> "$LOGFILE"
## #!/bin/bash
## Removes old revisions of snaps
## CLOSE ALL SNAPS BEFORE RUNNING THIS
#set -eu
#
#LANG=C snap list --all | awk '/disabled/{print $1, $3}' |#
    #while read snapname revision; do
      #  snap remove "$snapname" --revision="$revision"
    #done
##
#
## Upgrade flatpaks
(( N++ ))
echo "$N.: Upgrade flatpaks" >> "$LOGFILE"
echo "-----------------------------------------------" >> "$LOGFILE"
flatpak history >> "$LOGFILE"
flatpak remote-ls --updates >> "$LOGFILE"
sudo flatpak update -y >> /dev/null       ### logs blurb! 2part!
echo " " >> "$LOGFILE"
timestamp
echo "$TIMESTAMP" >> "$LOGFILE"
echo " " >> "$LOGFILE"
# Flatpak Cleanup
flatpak uninstall --unused -y >> /dev/null                       # log?
bash -c "! pgrep -x flatpak && rm -rv /var/tmp/flatpak-cache-*" >> /dev/null
# log? blurb!
# flatpak repair ?
# update_flatpak_cli.py                  ### Quelle! Über Bash startbar!
#
## Upgrade TeX Live
(( N++ ))
echo "$N.: Upgrade TeX Live" >> "$LOGFILE"
echo "-----------------------------------------------" >> "$LOGFILE"
sudo tlmgr update --self >> "$LOGFILE" # FIXME: tee!
sudo tlmgr update --all >> "$LOGFILE" # FIXME: tee!
echo " " >> "$LOGFILE"
timestamp
echo "$TIMESTAMP" >> "$LOGFILE"
echo " " >> "$LOGFILE"
# Cleanup?
#
echo "Software upgrade finished." >> "$LOGFILE"
echo " " >> "$LOGFILE"
#
## Check if reboot is neccessary
if [ -f /run/reboot-required ]; then
        echo "*** REBOOT REQUIRED ***" >> "$LOGFILE"
  else
        echo "No reboot required." >> "$LOGFILE"
fi
#
## Show logfile in stdout
echo " "
cat "$LOGFILE"
