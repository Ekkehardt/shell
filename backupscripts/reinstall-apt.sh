#! /bin/bash
# *** reinstall-apt.sh ***
# Description: "Installs packages via apt using package and state files created
#               by softwarelist.sh."
# Version: 1.1.0c
################################################################################
# Installs packages via apt using package and state files created
#               by 'softwarelist.sh'. Output is printed to STDOUT.
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-02-08
################################################################################
# *ONGOING* ->  Preparation for 1.2.0
#   Commit: "reinstall-apt: <TEXT>"
#       TODO: make similar lists for snap, flatpak
#       FIXME: file names do not match softwarelist V 0.7!
################################################################################
#
# TODO: init vars
# VERSION="1.2.0"
## Update package database
# TODO: determine date and machine
# TODO: check for existing list files
sudo apt-get update
## Install Packages
# TODO: only run if lists for THIS machine exist, run only latest or specified, else exit 1
xargs -a "packages.list" sudo apt-get install
## Mark auto-installed packages
xargs -a "packages.state" sudo apt-mark auto
echo "Done."
