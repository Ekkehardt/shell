#! /bin/bash
# *** softwarelist.sh ***
# Description: "Creates a comprehensive list of installed software."
# Version: 0.7.0
################################################################################
# This script creates a comprehensive list of installed software. This covers
#       apt/dpkg with sources lists, snaps, flatpaks, AppImages, Gnome Shell
#       Extensions, GoG games, Steam games and apps and TeX Live
#       version/location. For apt packages, a list of all package names and a
#       list of automatically installed are being created which may be used to
#       batch-reinstall packages using the included script 'reinstall-apt.sh'.
#
# Output is stored in './softwarelist-$MACHINE-$DATESTAMP.txt'.
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
# glpat-wNG1Vr3HwWMFMBRgN3T9
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-02-07s
################################################################################
# TODO: Lutris, Phoenicis PlayOnLinux;
#       Check and only list if present for each block;
#       FIXME: 01: L62 + L64: Output has too many quotes in softwarelist*.txt
#       FIXME: 02: L108ff + L158ff: path butchered
#       FIXME: 03: Ls112-177: remove for-loops (6)
#       FIXME: 04: Ls51-202: contract commands to groups (?) (21)s
#       FIXME: 05: L62, 2*: quoting/glob/splist
#       FIXME: 06: l181: REMOVE $() -> (RISK OF EXECUTING STRING)
################################################################################
#
## Variables init
DATESTAMP="1970-01-01"
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
OS="UNIX 1.0"
OUTFILE="softwarelist-$MACHINE-$DATESTAMP.txt"     ### Multiple files in future?
N=0
#
## Functions
datestamp() { DATESTAMP=$(date +"%F")
            }
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update Variables
datestamp
MACHINE=$(hostname)
USER=$(whoami)
OS=$(lsb_release -ds)
OUTFILE="softwarelist-$MACHINE-$DATESTAMP.txt"
#
## Output Header
echo "List of installed software in $OS on $MACHINE." > "$OUTFILE"
echo "Created by $USER@$MACHINE on $DATESTAMP." >> "$OUTFILE"
echo "================================================================================" >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## List of apt/dpkg packages
(( N++ ))
echo "$N.: List of packages installed through apt/dpkg" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
COLUMNS=200 dpkg-query -l >> "$OUTFILE"
echo " " >> "$OUTFILE"
dpkg --get-selections | awk '!/deinstall|purge|hold/ {print $1}' > "packages-$MACHINE-$DATESTAMP".list
echo "File packages-$MACHINE-$DATESTAMP\.list for reinstall via 'reinstall-apt.sh' has been created." >> "$OUTFILE"
apt-mark showauto > autoinstalls_"$MACHINE"_"$DATESTAMP".list
echo "File autoinstalls-$MACHINE-$DATESTAMP.list for reinstall via 'reinstall-apt.sh' has been created." >> "$OUTFILE"
echo " " >> "$OUTFILE"             ### Double newlines?
#                                  ### Similar lists for other package managers?
#
# Overview of apt sources
(( N++ ))
echo "$N.: Overview of apt sources" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
find /etc/apt/sources.list* -type f -name '*.list' -exec bash -c 'echo -e "\n## $1 ";grep "^[[:space:]]*[^#[:space:]]" ${1}' _ {} \; >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## List of snaps
(( N++ ))
echo "$N.: List of software installed through snap" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
snap list >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## List of flatpaks
(( N++ ))
echo "$N.: List of software installed through flatpak" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
flatpak list >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## List of AppImages
(( N++ ))
echo "$N.: List of $USER's AppImages" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
find "$HOME" -type f -name "*.AppImage" >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## List of GNOME Shell Extensions
(( N++ ))
echo "$N.: List of $USER's GNOME Shell Extensions" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
ls "$HOME"/.local/share/gnome-shell/extensions >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## List of GoG games                               ### Find alternate locations?
(( N++ ))
echo "$N.: List of $USER's GoG games" >> "$OUTFILE"
for i in $(find / -path "*/GOG\ Games" 2>/dev/null);
        do
            echo In Library "$i": >> "$OUTFILE";
            echo "----" >> "$OUTFILE";
            ls "$i" >> "$OUTFILE";
            echo " " >> "$OUTFILE";
        done
#
## List of Steam games
(( N++ ))
echo "$N.: List of $USER's Steam games and apps" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
for i in $(find / -path "*steamapps/common" 2>/dev/null);
        do
            echo In Library "$i": >> "$OUTFILE";
            echo "----" >> "$OUTFILE";
            ls "$i" >> "$OUTFILE";
            echo " " >> "$OUTFILE";
        done
#                                             ### Add Lutris, Wine, Bottles? ...
## List of Games folder
(( N++ ))
echo "$N.: List of $USER's 'Games' folder" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
for i in $(find / -path "/home/$USER/Games" 2>/dev/null);
        do
            echo In Library "$i": >> "$OUTFILE";
            echo "----" >> "$OUTFILE";
            ls "$i" >> "$OUTFILE";
            echo " " >> "$OUTFILE";
        done
#
## List of Herioc folder
(( N++ ))
echo "$N.: List of $USER's 'Heroic' folder" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
for i in $(find / -path "*Games/Heroic" 2>/dev/null);
        do
            echo In Library "$i": >> "$OUTFILE";
            echo "----" >> "$OUTFILE";
            ls "$i" >> "$OUTFILE";
            echo " " >> "$OUTFILE";
        done
#
# PlayOnLinux's virtual drives
(( N++ ))
echo "$N.: List of $USER's PlayOnLinux drives" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
for i in $(find / -path "*PlayOnLinux's\ virtual\ drives" 2>/dev/null);
        do
            echo In Library "$i": >> "$OUTFILE";
            echo "----" >> "$OUTFILE";
            ls "$i" >> "$OUTFILE";
            echo " " >> "$OUTFILE";
        done
#
# Bottles appliances
(( N++ ))
echo "$N.: List of $USER s Bottles Appliances" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
for i in $(find / -path "*com.usebottles.bottles/data/bottles/bottles" 2>/dev/null);
        do
            echo In Library "$i": >> "$OUTFILE";$(find / -path "*com.usebottles.bottles/data/bottles/bottles" 2>/dev/null)
            echo "----" >> "$OUTFILE";
            ls "$i" >> "$OUTFILE";
            echo " " >> "$OUTFILE";
        done
#
## State of TeX Live Install / version
(( N++ ))
echo "$N.: State of TeX Live install" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
tlmgr version >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## Python modules
(( N++ ))
echo "$N.: User-installed Python modules" >> "$OUTFILE"
echo "-----------------------------------------------" >> "$OUTFILE"
echo " " >> "$OUTFILE"
pip list -l >> "$OUTFILE"
echo " " >> "$OUTFILE"
#
## Output footer
timestamp
echo "================================================================================" >> "$OUTFILE"
echo "*** Last modified $TIMESTAMP by softwarelist.sh. ***" >> "$OUTFILE"
echo " " >> "$OUTFILE"
echo "EOF" >> "$OUTFILE"
echo " " >> "$OUTFILE"
