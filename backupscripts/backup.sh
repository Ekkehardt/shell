#! /bin/bash
# *** backup.sh ***
# Description: "Universal rsync backup script."
# Version: 0.6.2 # TODO: Make use of a variable!
################################################################################
# This script creates an incremental backup of /boot, /etc, /home, and
#       /datadisk on a mounted drive given by the user, using rsync. /datadisk
#       is peculiar to TUXEDO machines and may be omitted. Comes with an
#       ignorelist of paths that are not to be copied. This list is based on
#       <https://github.com/rubo77/rsync-homedir-excludes> by Ruben
#       Barkow-Kuder. A log of its last run is stored in
#       /$BUPATH/$LOGDIR/bu-$MACHINE.log'. Status messages are sent to stdout
#       and the file '$BUPATH/$LOGDIR/bu-$MACHINE.log'.
#
# This variant is interactive/machine-independent.
#       Thus called manually, but, hopefully, regularly.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 20.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-02-08
################################################################################
# *ONGOING* ->  Preparation for 1.0.0
#   Commit: "backup: <TEXT>"
################################################################################
# TODO: Status mit tee aufteilen oder Option der Unterschiedlichkeit nutzen?
#        -> -hPu! Statfile/stdout mit tee vereinheitlichen. Alles Englisch!
# TODO: Kein globales Changelog, sondern je Maschine --> Übersichtklichkeit!
# TODO: Partial und Temporary für ALLE gleich halten!
# TODO: Changelog: Pfad - Datum, Uhrzeit. Stand: Datum je Maschine.
# TODO: Optionen wählbar? Conf bei ersten Start erstellen (Abfrage) und laden
#        lassen? Spiele uä: nur Saves, nicht Daten sichern! Weitere Ausnahmen?
# TODO: Steam/common ist raus, aber saves behalten --- WIE?
# TODO: Changelog inInstalls packages via apt using package and state files
#        created by softwarelist.sh.
# TODO: globalen Ordner, eine Datei je Host. Analog: i2p, dl, ...
# TODO: Kommentare im Text unten! Logfile hübscher, siehe andere Skripte!
################################################################################
#
## Check for root permissions
if [[ $EUID -ne 0 ]]; then
   echo "The script must be run with root permissions!" # TODO: REDESIGN! sudo rsync!
   exit 1
fi
#
## Pre-define variables
# VERSION="1.0.0"
TIMESTAMP="1970-01-01, 00:00:00"
BUPATH="/media/frank/Backup_5TB/sleipnir"  # Full automation except drive name?
LOGDIR="z_bu-logs"                         # NO Whoami command! Other distros?
TEMPDIR="z_bu-temp"
PARTDIR="z_bu-part"
MACHINE="sleipnir"
USER="frank"
MOUNTPOINT="/media/frank/Backup_5TB/"
#
## Functions
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Determine mountpoint and hostname
MACHINE=$(hostname)
# USER=$(whoami)                   ### FIXME: Will work when script invokes sudo below.
timestamp
echo " "
echo "$TIMESTAMP"
echo "Backup script started for *$MACHINE*."
echo "Mountpoint of Your backup, (i.e '/media/frank/Backup_5TB/'):"
read -r MOUNTPOINT
## call list script here?
# FIXME: search for mounted BU disk and propose it here!
#                     ### How to have a preset confirmed by ENTER without input?
## Assemble backup path
BUPATH=$MOUNTPOINT/$MACHINE
#
## FIXME: Check if disks are mounted???
#  !!! Execute BU on available disks only! Else: messes up MPs!
#  !!! Check: /proc/mount & /proc/self/mount!?
#
## Set sync options
SYSOPT="-a --delete --delete-after --delete-excluded --info=progress2,stats2 --ignore-errors --temp-dir=$BUPATH/.$TEMPDIR --partial-dir=$BUPATH/.$PARTDIR --log-file=$BUPATH/$LOGDIR/bu-$MACHINE.log"
DATOPT="-auPmhx --delete --delete-during --delete-excluded --info=progress2,stats2 --ignore-errors --exclude-from=./ignorelist.txt --include=.torrent -temp-dir=$BUPATH/.$TEMPDIR --partial-dir=$BUPATH/.$PARTDIR --log-file=$BUPATH/$LOGDIR/bu-$MACHINE.log"
#  *** Note on rsync options ***
#     -a                               archive mode:
#     -v                               verbose: give more info
#     -z                               compress before transfer (remote)
#     -h                               hunan readable sizes
#     -P                               keep partial transfers
#     -u                               update: keep latest versions
#     --info=progress2                 show progress
#     --exclude=dir                    exclude given path
#     --exclude-from=ignorelist.txt    exclude paths given in txt file
#
#  ... add more!
#
#  FIXME: Ask to start here!
#  FIXME: State inputs/actions again!
#
## Make script directories, if necessary
mkdir -p "$BUPATH/$LOGDIR"
mkdir -p "$BUPATH/.$TEMPDIR"
mkdir -p "$BUPATH/.$PARTDIR"
#
## Create log/output headers
timestamp
# Log - entfällt sicher, wenn Option für rsync gut funktioniert.
echo " " > "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo "$TIMESTAMP" >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo "Backup for host *$MACHINE* is about to start." >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo "Writing incremental backup of '/boot', '/etc', '/home' and /datadisk to" >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo "path *$BUPATH*." >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo " " >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
# Stdout
echo " "
echo "$TIMESTAMP"
echo "Backup for host *$MACHINE* is about to start."
echo "Writing incremental backup of '/boot', '/etc', '/home' and /datadisk to"
# TODO: Script should ask, which dirs are to be synced.
echo "path *$BUPATH*."
echo " "
#
## Backup commands
timestamp
# /boot
echo "$TIMESTAMP" >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
rsync "$SYSOPT" /boot "$BUPATH" # log via option (?) !!! # FIXME, but how???
#                                   ### May be omitted, if log is done by rsync.
#                                   ### Options + tee: log+std or stat+std,
#                                        depending on formats!
#                                   ### Stat should be a copy of stdout, rsync
#                                        should write to log and stdout at once.
#                                   ### Ignorelist only for /home & /datadisk!
#                                        HOW for /boot? /etc? Google!
#                                   ### TODO: comment to log, file-lists external for each rsync run
#
# /etc
timestamp
echo "$TIMESTAMP" >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
rsync "$SYSOPT" /etc "$BUPATH" # FIXME, but how???
#
# /home
timestamp
echo "$TIMESTAMP" >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
rsync "$DATOPT" /home "$BUPATH" # FIXME, but how???
#
# /datadisk                                  ### Exists so far only on sleipnir!
#                                              # TODO: check or prompt for external storage!
timestamp
echo "$TIMESTAMP" >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
rsync "$DATOPT" /datadisk "$BUPATH" # FIXME, but how???
#
## Log/stdout footers
# Log
timestamp
echo " " >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo "Backup done." >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo "$TIMESTAMP" >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
echo " " >> "$BUPATH/$LOGDIR/bu-$MACHINE.log"
# Stdout
echo " "
echo "$TIMESTAMP"
echo "Backup done."
echo "Log and messages saved to folder $BUPATH/$LOGDIR/."
echo " "
echo "See You next week! ;)"                                  ### TODO: Calculate???
echo " "
#
## Final foo: statefile, logfile permissions
# Create statefile
timestamp
echo "Last backup of $MACHINE" > "$BUPATH/Last-BU.txt"        ### TODO: Variables! # what?!
echo "---------------------------" >> "$BUPATH/Last-BU.txt"
echo "$TIMESTAMP" >> "$BUPATH/Last-BU.txt"
echo " " >> "$BUPATH/Last-BU.txt"
# Set statefile permissions
chmod 644 "$BUPATH/Last-BU.txt"
# Set logfile permissions
chmod 644 "$BUPATH/$LOGDIR/bu-$MACHINE.log"
#
## call list script here?
