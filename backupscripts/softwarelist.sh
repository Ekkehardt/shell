#! /bin/bash
# *** softwarelist.sh ***
# Description: "Creates a comprehensive list of installed software."
# Version: 0.7.8 # TODO: Make use of a variable!
################################################################################
# This script creates a comprehensive list of installed software. This covers
#       apt/dpkg with sources lists, snaps, flatpak, AppImages, Gnome Shell
#       Extensions, GoG games, Steam games and apps and TeX Live
#       version/location. For apt packages, a list of all package names and a
#       list of automatically installed are being created which may be used to
#       batch-reinstall packages using the included script 'reinstall-apt.sh'.
#
# Output is stored in './softwarelist-$MACHINE-$DATESTAMP.txt'.
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-02-08
################################################################################
# *ONGOING* ->  Preparation for 1.0.0
#   Commit: "softwarelist: ..."
#       FIXME: 01: L108ff + L158ff: path butchered
#       FIXME: 02: Ls112-177: remove for-loops (6)
#       FIXME: 03: Ls51-202: contract commands to groups (?) (21)s
#       TODO:  01: Optimize output file names: $ISODATE_$CONTENT-$MACHINE.list
#                    etc. - consistent throughout!
#       TODO:  02: Lutris, Phoenicis PlayOnLinux;
#       TODO:  03: Check and only list if present for each block
################################################################################
#
## Variables init
VERSION="0.7.8"
DATESTAMP="1970-01-01"
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
OS="UNIX 1.0"
OUTFILE="softwarelist-$MACHINE-$DATESTAMP.txt"     ### Multiple files in future?
N=0
#
## Functions
datestamp() { DATESTAMP=$(date +"%F")
            }
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update Variables
datestamp
MACHINE=$(hostname)
USER=$(whoami)
OS=$(lsb_release -ds)
OUTFILE="softwarelist-$MACHINE-$DATESTAMP.txt"
#
{ ## Output Header
    echo "Script softwarelist $VERSION - list of installed software in $OS on $MACHINE."
    echo "Created by $USER@$MACHINE on $DATESTAMP."
    echo "================================================================================"
    echo
} > "$OUTFILE"
#
(( N++ ))
{ ## List of apt/dpkg packages
    echo "$N.: List of packages installed through apt/dpkg"
    echo "-----------------------------------------------"
    echo " "
    COLUMNS=200 dpkg-query -l
    echo " "
    dpkg --get-selections | awk '!/deinstall|purge|hold/ {print $1}' > "packages-$MACHINE-$DATESTAMP".list
    echo "File packages-$MACHINE-$DATESTAMP.list for reinstall via 'reinstall-apt.sh' has been created."
    apt-mark showauto > "autoinstalls-$MACHINE-$DATESTAMP.list"
    echo "File autoinstalls-$MACHINE-$DATESTAMP.list for reinstall via 'reinstall-apt.sh' has been created."
    echo " "
    ### TODO: Double newlines?
    ### Similar lists for other package managers?
} >> "$OUTFILE"
#
# Overview of apt sources
(( N++ ))
{
    echo "$N.: Overview of apt sources"
    echo "-----------------------------------------------"
    find /etc/apt/sources.list* -type f -name '*.list' -exec bash -c 'echo -e "\n## $1 ";grep "^[[:space:]]*[^#[:space:]]" ${1}' _ {} \;
    echo " "
} >> "$OUTFILE"
#
(( N++ ))
{ ## List of snaps
    echo "$N.: List of software installed through snap"
    echo "-----------------------------------------------"
    echo " "
    snap list
    echo " "
} >> "$OUTFILE"
#
(( N++ ))
{ ## List of flatpaks
    echo "$N.: List of software installed through flatpak"
    echo "-----------------------------------------------"
    echo " "
    flatpak list
    echo " "
} >> "$OUTFILE"
#
(( N++ ))
{ ## List of AppImages
    echo "$N.: List of $USER's AppImages"
    echo "-----------------------------------------------"
    echo " " >> "$OUTFILE"
    find "$HOME" -type f -name "*.AppImage"
    echo " "
} >> "$OUTFILE"
#
(( N++ ))
{ ## List of GNOME Shell Extensions
    echo "$N.: List of $USER's GNOME Shell Extensions"
    echo "-----------------------------------------------"
    echo " "
    ls "$HOME"/.local/share/gnome-shell/extensions
    echo " "
} >> "$OUTFILE"
#
(( N++ ))
{ ## List of GoG games ### TODO: Find alternate locations
    echo "$N.: List of $USER's GoG games"
    echo "-----------------------------------------------"
    echo " "
    for i in $(find / -path "*/GOG\ Games" 2>/dev/null) -exec;
            do
                echo "In Library $i:";
                echo "----";
                ls "$i";
                echo " ";
            done
} >> "$OUTFILE"
#
(( N++ ))
{ ## List of Steam games
    echo "$N.: List of $USER's Steam games and apps"
    echo "-----------------------------------------------"
    echo " "
    for i in $(find / -path "*steamapps/common" 2>/dev/null) -exec;
            do
                echo "In Library $i:";
                echo "----";
                ls "$i";
                echo " ";
            done
} >> "$OUTFILE"
#
### TODO: Add Lutris, Wine, .. ???
(( N++ ))
{ ## List of Games folder
    echo "$N.: List of $USER's 'Games' folder" >> "$OUTFILE"
    echo "-----------------------------------------------"
    echo " "
    for i in $(find / -path "/home/$USER/Games" 2>/dev/null) -exec;
            do
                echo "In Library $i:";
                echo "----";
                ls "$i";
                echo " ";
            done
} >> "$OUTFILE"
#
(( N++ ))
{ ## List of Heroic folder
    echo "$N.: List of $USER's 'Heroic' folder"
    echo "-----------------------------------------------"
    echo " "
    for i in $(find / -path "*Games/Heroic" 2>/dev/null) -exec;
            do
                echo "In Library $i:";
                echo "----";
                ls "$i";
                echo " ";
            done
} >> "$OUTFILE"
#
(( N++ ))
{ ## PlayOnLinux's virtual drives
    echo "$N.: List of $USER's PlayOnLinux drives"
    echo "-----------------------------------------------"
    echo " "
    for i in $(find / -path "*PlayOnLinux's\ virtual\ drives" 2>/dev/null) -exec;
            do
                echo "In Library $i:";
                echo "----";
                ls "$i";
                echo " ";
            done
} >> "$OUTFILE"
#
(( N++ ))
{ ## Bottles appliances
    echo "$N.: List of $USER s Bottles Appliances"
    echo "-----------------------------------------------"
    echo " "
    for i in $(find / -path "*com.usebottles.bottles/data/bottles/bottles" 2>/dev/null) -exec;
            do
                echo "In Library $i:";
                echo "----";
                ls "$i";
                echo " ";
            done
}  >> "$OUTFILE"
#
(( N++ ))
{ ## State of TeX Live Install / version
    echo "$N.: State of TeX Live install"
    echo "-----------------------------------------------"
    echo " "
    tlmgr version
    echo " "
} >> "$OUTFILE"
#
(( N++ ))
{ ## Python modules
    echo "$N.: User-installed Python modules"
    echo "-----------------------------------------------"
    echo " "
    pip list -l
    echo " "
} >> "$OUTFILE"
#
timestamp
{ ## Output footer
    echo "================================================================================"
    echo "*** Last modified $TIMESTAMP by softwarelist.sh. ***" # TODO: add Version from a variable set in header!
    echo " "
    echo "EOF"
    echo " "
} >> "$OUTFILE"
