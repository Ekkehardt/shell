# !/bin/bash
# A script to set some keybord options on a Tuxedo DX1707 notebook using xdotool
# 1st: toggle touchpad off
xdotool key --clearmodifiers XF86TouchpadToggle # from 'xmodmap -pke'
#xdotool key --clearmodifiers XF86KbdLightOnOff
