#! /bin/bash
# *** autoupgrade.sh ***
# Description: "Complete upgrade of Ubuntu machines."
# Version: 0.4.4
################################################################################
# This script fetches and installs all available upgrades through apt, the
#       Tex Live manager, flatpak and the snap system. After application it
#       carries out a clean-up.
#
# Should reside in ~/scripts. Will create a logfile in ~/scripts/mylogs.
#
# May be enabled with 'sudo crontab -e';
#       '@reboot /root/scripts/autoupgrade.sh'
#       '37 1 * * * /root/scripts/autoupgrade.sh'
#       Or run via alias: alias full-upgrade='$HOME/scripts/autoupgrade.sh'
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2022-09-13
################################################################################
# TODO: Relative Paths? Cleanup for flat/snap? Root-Check w/warning and
#       break bc whoami? Check and only do what's neccessary?
################################################################################
#
## Variables init
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
LOGPATH="/home/user/scripts/mylogs/autoupgrade.log"
N=0
#
## Functions
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update Variables
timestamp
MACHINE=$(hostname)
USER=$(whoami)
LOGPATH=$HOME/scripts/mylogs/autoupgrade.log
#
## Make Logdir, if not already done
DIR="$HOME/scripts/mylogs"
if [ -d "$DIR" ]; then
  echo "Logdir already present." >> /dev/null
  ### Nothing to do ###
  else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $HOME/scripts/mylogs/
  echo "Logdir created." >> /dev/null
fi
# 
## Check if repos are online
while true; do ping -c1 www.ubuntu.com &> /dev/null && break; done
#
## Logfile init
echo " "
echo "Last software upgrade started by $USER@$MACHINE $TIMESTAMP." > $LOGPATH
echo "================================================================================" >> $LOGPATH
echo " " >> $LOGPATH
#
## Apt stuff
let N++
echo "$N.: Upgrade packages installed through apt/dpkg" >> $LOGPATH
echo "-----------------------------------------------" >> $LOGPATH
# Update package list
sudo apt-get update >> $LOGPATH
echo " " >> $LOGPATH
# Upgrade all debs
sudo apt-get dist-upgrade -y >> $LOGPATH
echo " " >> $LOGPATH
# Remove debs no longer in repos
sudo apt-get autoremove -y >> $LOGPATH
echo " " >> $LOGPATH
# Clean local package cache
sudo apt-get clean -y >> $LOGPATH
timestamp
echo $TIMESTAMP >> $LOGPATH
echo " " >> $LOGPATH
#
## Upgrade snaps
let N++
echo "$N.: Upgrade snaps" >> $LOGPATH
echo "-----------------------------------------------" >> $LOGPATH
sudo snap refresh # >> $LOGPATH # logs blurb! why?
snap changes >> $LOGPATH
# sudo rm /var/lib/snapd/cache/*        ### Does not exist. Where else cache?
                                        ### Other cleanup?
echo "Output of 'snap refresh' could not be redirected to log file!" >> $LOGPATH
echo " " >> $LOGPATH
timestamp
echo $TIMESTAMP >> $LOGPATH
echo " " >> $LOGPATH
#
## Upgrade flatpaks
let N++
echo "$N.: Upgrade flatpaks" >> $LOGPATH
echo "-----------------------------------------------" >> $LOGPATH
sudo flatpak update >> $LOGPATH
echo " " >> $LOGPATH
timestamp
echo $TIMESTAMP >> $LOGPATH
echo " " >> $LOGPATH
# Cleanup?
#
## Upgrade TeX Live
let N++
echo "$N.: Upgrade TeX Live" >> $LOGPATH
echo "-----------------------------------------------" >> $LOGPATH
sudo tlmgr update --self >> $LOGPATH
sudo tlmgr update --all >> $LOGPATH
echo " " >> $LOGPATH
timestamp
echo $TIMESTAMP >> $LOGPATH
echo " " >> $LOGPATH
# Cleanup?
#
echo "Software upgrade finished." >> $LOGPATH
echo " " >> $LOGPATH
#
## Check if reboot is neccessary
if [ -f /run/reboot-required ]; then        
        echo "*** REBOOT REQUIRED ***" >> $LOGPATH
   else
        echo "No reboot required." >> $LOGPATH
fi
#
## Show logfile in stdout
echo " "
cat $LOGPATH

