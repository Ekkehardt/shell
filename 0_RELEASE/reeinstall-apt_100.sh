#! /bin/bash
# *** reinstall-apt.sh ***
# Description: "Installs packages via apt using package and state files created
#               by softwarelist.sh."
# Version: 1.0.0
################################################################################
# Installs packages via apt using package and state files created
#               by 'softwarelist.sh'. Output is printed to STDOUT.
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2022-09-13
################################################################################
# TODO: 
################################################################################
#
## Update package database
sudo apt-get update
## Install Packages
xargs -a "packages.list" sudo apt-get install
## Mark auto-installed packages
xargs -a "packages.state" sudo apt-mark auto
echo "Done."

