#! /bin/bash
# *** softwarelist.sh ***
# Description: "Creates a comprehensive list of installed software."
# Version: 0.5.7
################################################################################
# This script creates a comprehensive list of installed software. This covers
#       apt/dpkg with sources lists, snaps, flatpaks, AppImages, Gnome Shell
#       Extensions, GoG games, Steam games and apps and TeX Live
#       version/location. For apt packages, a list of all package names and a
#       list of automatically installed are being created which may be used to
#       batch-reinstall packages using the included script 'reinstall-apt.sh'.
#
# Output is stored in './softwarelist-$MACHINE-$DATESTAMP.txt'.
#
# This variant is machine-independent.
#
# Tested on a TUXEDO DX1707 laptop with Ubuntu 22.04.
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2022-09-13
################################################################################
# TODO: Check and only list if present for each block;
################################################################################
#
## Variables init
DATESTAMP="1970-01-01"
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
OS="UNIX 1.0"
OUTFILE="softwarelist-$MACHINE-$DATESTAMP.txt"     ### Multiple files in future?
N=0
#
## Functions
datestamp() { DATESTAMP=$(date +"%F")
            }
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update Variables
datestamp
MACHINE=$(hostname)
USER=$(whoami)
OS=$(lsb_release -ds)
OUTFILE="softwarelist-$MACHINE-$DATESTAMP.txt"
#
## Output Header
echo "List of installed software in $OS on $MACHINE." > $OUTFILE
echo "Created by $USER@$MACHINE on $DATESTAMP." >> $OUTFILE
echo "================================================================================" >> $OUTFILE
echo " " >> $OUTFILE
#
## List of apt/dpkg packages
let N++
echo "$N.: List of packages installed through apt/dpkg" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo " " >> $OUTFILE
COLUMNS=200 dpkg-query -l >> $OUTFILE
echo " " >> $OUTFILE
dpkg --get-selections | awk '!/deinstall|purge|hold/ {print $1}' > packages.list
echo "File 'packages.list' for reinstall via 'reinstall-apt.sh' has been created." >> $OUTFILE
apt-mark showauto > packages.state
echo "File 'packages.state' for reinstall via 'reinstall-apt.sh' has been created." >> $OUTFILE
echo " " >> $OUTFILE
#                                  ### Similar lists for other package managers?
#
# Overview of apt sources
let N++
echo "$N.: Overview of apt sources" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
find /etc/apt/sources.list* -type f -name '*.list' -exec bash -c 'echo -e "\n## $1 ";grep "^[[:space:]]*[^#[:space:]]" ${1}' _ {} \; >> $OUTFILE
echo " " >> $OUTFILE
#
## List of snaps
let N++
echo "$N.: List of software installed through snap" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo " " >> $OUTFILE
snap list >> $OUTFILE
echo " " >> $OUTFILE
#
## List of flatpaks
let N++
echo "$N.: List of software installed through flatpak" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo " " >> $OUTFILE
sudo flatpak list >> $OUTFILE
echo " " >> $OUTFILE
#
## List of AppImages
let N++
echo "$N.: List of $USER's AppImages" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo " " >> $OUTFILE
find $HOME -type f -name "*.AppImage" >> $OUTFILE
echo " " >> $OUTFILE
#
## List of GNOME Shell Extensions
let N++
echo "$N.: List of $USER's GNOME Shell Extensions" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo " " >> $OUTFILE
ls $HOME/.local/share/gnome-shell/extensions >> $OUTFILE
echo " " >> $OUTFILE
#
## List of GoG games                               ### Find alternate locations?
let N++
echo "$N.: List of $USER's GoG games" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo "*** NOTE *** Multiple idendical entries may occur due to symbolic links!" >> $OUTFILE
echo " " >> $OUTFILE
IFS="
" # Don't seperate at spaces! LB set as internal field separator.
for i in $(find / -path "*/GOG\ Games" 2>/dev/null);
        do
            echo In Library $i: >> $OUTFILE;
            echo "----" >> $OUTFILE;
            ls $i >> $OUTFILE;
            echo " " >> $OUTFILE;
        done
#
## List of Steam games
let N++
echo "$N.: List of $USER's Steam games and apps" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo " " >> $OUTFILE
for i in $(find / -path "*steamapps/common" 2>/dev/null);
        do
            echo In Library $i: >> $OUTFILE;
            echo "----" >> $OUTFILE;
            ls $i >> $OUTFILE;
            echo " " >> $OUTFILE;
        done
#
## State of TeX Live Install / version
let N++
echo "$N.: State of TeX Live install" >> $OUTFILE
echo "-----------------------------------------------" >> $OUTFILE
echo " " >> $OUTFILE
tlmgr version >> $OUTFILE
echo " " >> $OUTFILE
#
## Output footer
timestamp
echo "================================================================================" >> $OUTFILE
echo "*** Last modified $TIMESTAMP by softwarelist.sh. ***" >> $OUTFILE
echo " " >> $OUTFILE
echo "EOF" >> $OUTFILE
echo " " >> $OUTFILE

