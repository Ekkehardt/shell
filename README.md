# Shell scripts

A few small (and not so small) scripts for Bourne Again Shell (`bash`).

Scripts in RELEASE folder should be quite useable.

## Features

`autoupgrade.sh`: Complete upgrade of Ubuntu machines. Apt, snap, flatpak, TeX Live.

`backup.sh`: Universal rsync backup script. Comes with ignore list.

**DEFUNCT** `reinstall-apt.sh`: Installs packages via apt using package and state files created by `softwarelist.sh`.

`softwarelist.sh`: Creates a comprehensive list of installed software. Covers apt, snap, flatpak, GOG, Steam, TeX Live.

`photowizard.sh`: A script to automate a few standard image opüerations with ImageMagick (whgite balnce, despeckle, sharpen).

`publicip.sh`: This script determines your external IP and returns only the 4*3 number. Used by [my conky theme](https://gitlab.com/Ekkehardt/conky-orcrist).

`updates.sh`: This script determines the number of packages that are upgradeable by apt and returns only the number. Used by [my conky theme](https://gitlab.com/Ekkehardt/conky-orcrist).

`ctan-sync.sh`: Automatically sync a local mirror with CTAN servers using rsync and write a log file. Used for my own mirror on a Raspberry Pi 4.

`piholeport.sh`: Pihole does not yet support changing ports via external.conf, hence this workaround. Set up as a cronjob after sysupdate.

`zwick-lichten.sh`: This script is a simple notch filter for ASCII raw data produced by ZWICK 1476 tensile testing machines. Data provided in a 'data.txt' file are reduced to the file header and every 100th line. The results are stored in the file 'output.txt'.

`msat-average.sh`: This script reads ASCII raw data of Metis MSAT magnetic measurement devices from a 'data.txt' file and only saves lines with average (mean of four) values to an 'output.txt'file.

**DISCONTINUED** `logitech`: Workaround for Logitech wireless mice at Tuxedo DX1707 notebook in Lubuntu 18.04.

**DISCONTINUED** `touchpad`: Workaround for the Tuxedo DX1707 notebook touchpad in Lubuntu 18.04.

**DISCONTINUED** `keyset.sh`: A script to set some keyboard options on a Tuxedo DX1707 notebook using xdotool in Lubuntu 18.04.

**DISCONTINUED** `obx-propfilter`: This script is a filter for the output of `obxprop`. It scans for window properties needed to configure window behaviour with a `~/.config/openbox/lubuntu-rc.xml`-file in Lubuntu 18.04.

## Systems

Linux, BSD, Cygwin

## Technical information

Tested with `bash` in Ubuntu 22.04 LTS and Raspberry Pi OS 11 (Bullseye) respectively.

## Legal information

Licensed under MIT License conditions; see file `LICENSE`.

E. Frank Sandig, 2023-03-09
