#! /bin/bash
# *** ctan-sync.sh ***
# Description: "Automatic sync local mirror with CTAN and write log."
# Version: 1.0.0
################################################################################
# This script uses the 'official' online sync call for CTAN mirror sync,
#       but additionally writes a log file in order to review the last call.
#
# Should be enabled with 'sudo crontab -e';
#       '14 3 * * * /home/pi/scripts/ctan-sync.sh'
#       '@reboot /home/pi/scripts/ctan-sync.sh'
#
# This variant is for my local CTAN mirror on a Raspberry Pi.
# Tested on a Raspberry Pi 4 (4GB) running Raspbian GNU/Linux 10 (buster).
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2022-09-13
################################################################################
#
## Define variables
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
LOGFILE="/home/user/scripts/mylogs/ctan-sync.log"
#
## Functions
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update variables            
timestamp
MACHINE=$(hostname)
USER=$(whoami)
LOGFILE="/home/pi/scripts/mylogs/ctan-sync.log"
#
## Logfile init
echo "Last sync of local mirror to CTAN started by $USER@$MACHINE $TIMESTAMP." > $LOGFILE
echo "================================================================================" >> $LOGFILE
#
## Do we have the Interwebz?
while true; do ping -c1 www.ubuntu.com &> /dev/null && break; done
#
## The actual sync one-liner
rsync -av --delete rsync://rsync.dante.ctan.org/CTAN /var/ftp/pub/tex-archive >> $LOGFILE
#
## Footer
timestamp
echo " " >> $LOGFILE
echo "================================================================================" >> $LOGFILE
echo "Sync of local mirror to CTAN finished $TIMESTAMP." >> $LOGFILE

