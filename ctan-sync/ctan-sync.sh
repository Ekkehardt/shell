#! /bin/bash
# *** ctan-sync.sh ***
# Description: "Automatic sync local mirror with CTAN and write log."
# Version: 1.1.1
################################################################################
# This script uses the 'official' online sync call for CTAN mirror sync,
#       but additionally writes a log file in order to review the last call.
#
# Should be enabled with 'sudo crontab -e';
#       '14 3 * * * /home/pi/scripts/ctan-sync.sh'
#       '@reboot /home/pi/scripts/ctan-sync.sh'
#
# This variant is for my local CTAN mirror on a Raspberry Pi.
# Tested on a Raspberry Pi 4 (4GB) running Raspbian GNU/Linux 11 (Bullseye).
#
# E. Frank Sandig, schriftsatz@sandig-fg.de                           2023-02-08
################################################################################
# *ONGOING* ->  Preparation for 1.2.0
#   Commit: "ctan-sync: <TEXT>"
################################################################################
#
## Define variables
VERSION="1.1.1"
TIMESTAMP="1970-01-01, 00:00:00"
MACHINE="computer"
USER="user"
LOGFILE="/home/user/scripts/mylogs/ctan-sync.log"
#
## Functions
timestamp() { TIMESTAMP=$(date +"%F, %X")
            }
#
## Update variables
timestamp
MACHINE=$(hostname)
USER=$(whoami)
LOGFILE="/home/pi/scripts/mylogs/ctan-sync.log" # TODO: check if exists, else create???
#
## Logfile init
{
echo "Script ctan-sync $VERSION for local mirror to CTAN started by $USER@$MACHINE $TIMESTAMP."
echo "================================================================================"
} > "$LOGFILE"
#
## Do we have the Interwebz?
while true; do ping -c1 www.ubuntu.com &> /dev/null && break; done ### TODO better option?
#
## The actual sync one-liner
rsync -av --delete rsync://rsync.dante.ctan.org/CTAN /data/ftp/pub/tex-archive >> $LOGFILE
#
## Footer
timestamp
{
echo " "
echo "================================================================================"
echo "Sync of local mirror to CTAN finished $TIMESTAMP."
} >>  "$LOGFILE"
