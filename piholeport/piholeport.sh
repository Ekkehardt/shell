#! /bin/bash
# pihole does not yet support changing ports via external.conf
# hence this workaround
# as a cronjob after sysupdate
#
# upgrade (check) pihole
pihole -up
# change port
sed -ie 's/= 80/= 81/g' /etc/lighttpd/lighttpd.conf
# restart webserver
/etc/init.d/lighttpd restart
